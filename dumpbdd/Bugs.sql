SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: TestBugs
--
CREATE DATABASE Bugs;
USE Bugs; 
-- --------------------------------------------------------

--
-- Structure de la table projet
--

CREATE TABLE IF NOT EXISTS projet (
  id_projet int(11) AUTO_INCREMENT,
  nom_projet varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (id_projet)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table developpeur
--
CREATE TABLE IF NOT EXISTS developpeur (
  id_developpeur int(11) AUTO_INCREMENT,
  nom_developpeur varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  id_projet int(11),
  PRIMARY KEY (id_developpeur),
  FOREIGN KEY (id_projet) REFERENCES projet(id_projet)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;


-- --------------------------------------------------------

--
-- Structure de la table scrummaster
--
CREATE TABLE IF NOT EXISTS scrummaster (
  id_scrumaster int(11),
  id_proj int(11),
  date_scrummaster datetime,
  PRIMARY KEY (id_scrumaster, id_proj),
  FOREIGN KEY (id_proj) REFERENCES projet(id_projet),
  FOREIGN KEY (id_scrumaster) REFERENCES developpeur(id_developpeur)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;


-- --------------------------------------------------------

--
-- Structure de la table bug
--

CREATE TABLE IF NOT EXISTS bug (
  id_bug int(11) AUTO_INCREMENT,
  description_bug varchar(255) NOT NULL,
  created_bug datetime NOT NULL,
  status_bug varchar(255) NOT NULL,
  id_correcteur int(11) DEFAULT NULL,
  id_reporteur int(11) DEFAULT NULL,
  PRIMARY KEY (id_bug),
  FOREIGN KEY (id_correcteur) REFERENCES developpeur(id_developpeur),
  FOREIGN KEY (id_reporteur) REFERENCES developpeur(id_developpeur)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;


